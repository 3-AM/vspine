$(document).ready(function() {
	$(this).on('click', function(e){
		var target = e.target;
		var targetId = target.getAttribute('id');
		var targetNode = null;

		if (targetId == 'menu-toggler' || target.parentNode.getAttribute('id') == "menu-toggler"){
			$('#menu').toggleClass('active');
			return;
		}

		if (targetId == 'head-search-toggler'){
			$('#head-search').slideToggle(100);
			return;
		}

		if (targetId !== 'menu'){

			while (targetId !== 'menu' && targetId !=='page-wrapper' && targetId !=='foot' && targetId !=='head'){
				target = target.parentNode;
				if (target.hasAttribute('id')){
					targetId = target.getAttribute('id');
				}
			}

			if (!(targetId == 'menu')){
				$('#menu').removeClass('active');
			}
		} 
	});
});